//снежинки
let marginBottom;
let marginRight;
let snowMax = 100;
let snowColor = new Array("#AAAACC", "#DDDDFF", "#CCCCDD")
let snowLetter = "*";
let speed = 0.7;
let maxSize = 30;
let minSize = 8;

let snow = new Array();

function random(range) {
    rand = Math.floor(range*Math.random());
    return rand;
}

function initSnow() {
        marginBottom = window.innerHeight;
        marginRight = window.innerWidth;
    let snowSize = maxSize - minSize;

    for (i = 0; i <= snowMax; i++) {
        snow[i] = document.getElementById("snow"+i);
        snow[i].size = random(snowSize) + minSize;
        snow[i].style.fontSize = snow[i].size + "px";
        snow[i].style.color = snowColor[random(snowColor.length)];
        snow[i].sink = speed * snow[i].size / 5;
        snow[i].posX = random(marginRight - snow[i].size)
        snow[i].posY = random(2 * marginBottom - marginBottom - 2 * snow[i].size);
        snow[i].style.left = snow[i].posX + "px";
        snow[i].style.top = snow[i].posY + "px";
    }
    moveSnow();
}

function moveSnow() {
    for(i = 0; i <= snowMax; i++) {
        snow[i].posY += snow[i].sink;
        snow[i].style.left = snow[i].posX;
        snow[i].style.top = snow[i].posY + "px";
        if (snow[i].posY >= marginBottom - 2 * snow[i].size) {
         snow[i].posX = random(marginRight - snow[i].size)
            snow[i].posY = 0;
        }
    }
    setTimeout("moveSnow()",50);
}

for (i = 0; i <= snowMax; i++) {
    document.write("<span id='snow"+i+"' style='position:absolute; top:-"+ maxSize +"px;'>" + snowLetter + "</span>");
}

initSnow();
